*** Settings ***
Documentation    Is it Friday yet (ko)
Metadata         ID                           2462
Metadata         Reference                    03
Metadata         Automation priority          null
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Is it Friday yet (ko)
    [Documentation]    Is it Friday yet (ko)

    Given today is Monday
    When I ask whether it's Friday
    Then I should be told Yep


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_2462_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_2462_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =         Get Variable Value    ${TEST_SETUP}
    ${TEST_2462_SETUP_VALUE} =    Get Variable Value    ${TEST_2462_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_2462_SETUP_VALUE is not None
        Run Keyword    ${TEST_2462_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_2462_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_2462_TEARDOWN}.

    ${TEST_2462_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_2462_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =         Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_2462_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_2462_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END
